<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;

    protected $table= 'products';

    protected $fillable = [
        'name',
        'price',
        'quantity',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function withFilters($query, $properties)
    {
        foreach ($properties as $property => $values) {
            $query->whereHas('properties', function ($query) use ($property, $values) {
                $query->whereIn('name', $property)->whereIn('value', $values);
            });
        }

        return $query;
    }

    public function properties(): BelongsToMany
    {
        return $this->belongsToMany(Property::class)
            ->withPivot('value');
    }
}
