<?php

namespace App\Http\Services;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserService
{
    public function registerUser($request): JsonResponse
    {
        try {
            $validateUser = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'phone' => 'required|unique:users',
                'password' => 'required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d$%&!:.])/|confirmed',
                'password_confirmation' => 'required|same:password',
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
            $request['password'] = Hash::make($request['password']);
            $user = User::create(
                $request->only('name', 'phone', 'email', 'password')
            );

            return response()->json([
                'status' => true,
                'message' => 'User creating success',
                'token' => $this->createUserToken($user),
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);

        };
    }

    public function loginUser($request): JsonResponse
    {
        $field = filter_var($request->login, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        if (Auth::attempt([$field => $request->login, 'password' => $request->password])) {
            $user = Auth::user();
            $token = $this->createUserToken($user);
            return response()->json([
                'status' => true,
                'message' => 'User logged in',
                'token' => $token
            ]);
        } else {
            return response()->json([
                'status' => false,
                'error' => 'Unauthenticated'
            ], 401);
        }
    }

    public function logout($request): JsonResponse
    {
        $request->user()->token()->revoke();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function createUserToken($user)
    {
        return $user->createToken($user->name)->plainTextToken;
    }

    public function getUserToken(): JsonResponse
    {
        $user = Auth::user();
        $token = $this->createUserToken($user);

        return response()->json([
            'status' => true,
            'message' => 'User creating success',
            'token' => $token,
        ], 200);
    }
}
