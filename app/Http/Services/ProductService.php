<?php

namespace App\Http\Services;

use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\JsonResponse;

class ProductService
{
    public function getProducts(?array $properties): JsonResponse
    {

        // Примените фильтрацию на основе полученных свойств

        if ($properties) {
            $products = Product::withFilters($properties)->paginate(40);
        } else {
            $products = Product::paginate(40);
        }
        return response()->json($products);
    }
}
