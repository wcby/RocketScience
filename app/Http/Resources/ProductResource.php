<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'properties' => PropertyResource::collection($this->properties),
        ];
    }

    public function with($request): array
    {
        return [
            'status' =>'success'
        ];
    }

    public function withResponse($request, $response)
    {
        $response->header('Accept','application/json');
    }
}
