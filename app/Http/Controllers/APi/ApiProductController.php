<?php

namespace App\Http\Controllers\APi;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use App\Http\Services\ProductService;
use App\Http\Services\UserService;
use App\Models\Product;
use Illuminate\Http\Request;

class ApiProductController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index(Request $request)
    {
        return $this->productService->getProducts($request->only('properties'));
    }
}
