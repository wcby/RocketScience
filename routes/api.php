<?php

use App\Http\Controllers\APi\ApiProductController;
use App\Http\Controllers\APi\ApiUserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [ApiUserController::class, 'register']);
Route::post('login', [ApiUserController::class, 'login']);

Route::group([
   'middleware' => 'auth:sanctum'
], function(){
    Route::get('products', [ApiProductController::class, 'index']);
    Route::group([
        'prefix' => '/user',
    ],function(){
        Route::get('/', [ApiUserController::class, 'user']);
        Route::get('/logout', [ApiUserController::class, 'logout']);
    });
});
