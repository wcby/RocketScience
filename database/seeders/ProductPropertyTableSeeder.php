<?php

namespace Database\Seeders;

use App\Models\ProductProperty;
use Illuminate\Database\Seeder;

class ProductPropertyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductProperty::factory()->count(400)->create();
    }
}
