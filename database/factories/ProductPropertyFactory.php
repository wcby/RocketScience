<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductPropertyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'product_id' => function () {
                return Product::inRandomOrder()->first()->id;
            },
            'property_id' => function () {
                return Property::inRandomOrder()->first()->id;
            },
            'value' => $this->faker->word(1),
        ];
    }
}
