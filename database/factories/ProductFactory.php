<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(2),
            'price' => $this->faker->randomFloat(2, 100, 10000),
            'quantity' => $this->faker->numberBetween(0, 9)
        ];
    }
}
